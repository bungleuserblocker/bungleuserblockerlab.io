cl=console.log

hide_posts_rule='article[data-user-id="{user_id}"] div{ width: 0px; height: 0px; overflow: hidden;}'
placeholder_rule='\narticle[data-user-id="{user_id}"]:after{ content:"Post hidden"}'

function flip(obj) {
    obj.setAttribute("state",obj.getAttribute("state") ^ 1)
}

function loadJSON(callback) {
    var xhr = new XMLHttpRequest();
    xhr.overrideMimeType("application/json");
    xhr.open('GET', '0.json');
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200){
           callback(xhr.responseText)
        }
    }
    xhr.send()
}

function populateUserlistCallback(response) {
    var members_data=JSON.parse(response)['members'],
        users_list=document.getElementById('users_list'),
        current_date=new Date("2017-04-19T00:00:00.000Z")
    for (let member of members_data) {
        let last_posted_at=new Date(member['last_posted_at'])
        if ((current_date-last_posted_at)<2592000000) {
            let text_node=document.createTextNode(member['username'])
            let new_element=document.createElement('div')
            new_element.appendChild(text_node)
            new_element.setAttribute('onclick','flip(this)')
            new_element.setAttribute('class','toggleable user')
            new_element.setAttribute('user_id',member['id'])
            new_element.setAttribute('state',0)
            users_list.appendChild(new_element)
        }
    }
}

function populateUserlist() {
    loadJSON(populateUserlistCallback)
}

function generateCss(obj) {
    var rule_list=[]
    rule_list.push('@namespace url(http://www.w3.org/1999/xhtml);@-moz-document domain("bungle.cafe") {')
    var rule_template=hide_posts_rule
    if (document.getElementById("options_placeholder").getAttribute('state') & 1) {
        rule_template+=placeholder_rule
    }
    for (let selected_member of document.querySelectorAll(".user[state='1']")) {
        let user_id = selected_member.getAttribute('user_id')
        let rule = rule_template.replace(/\{user_id\}/g,user_id)
        rule_list.push(rule)
    }
    rule_list.push('}')
    rules=rule_list.join('\n')
    rules_data_uri='data:text/css;base64,'+btoa(rules)
    let text_node=document.createTextNode("Your CSS is ready! Click here to open it."),
        new_element=document.createElement('a'),
        parent_node=document.getElementById('result_container')
    parent_node.innerHTML = ''
    new_element.appendChild(text_node)
    new_element.setAttribute('href',rules_data_uri)
    new_element.setAttribute('class','toggleable')
    parent_node.appendChild(new_element)
    location.href='#'
    location.href='#result_container'
}




populateUserlist()
if (navigator.userAgent.includes("Firefox")) {
    document.getElementById('instructions_chrome').setAttribute("hidden","1")
   }
else{
    document.getElementById('instructions_firefox').setAttribute("hidden","1")
}

/*
function reqListener () {
  console.log(this.responseText);
  }

var oReq = new XMLHttpRequest();
oReq.addEventListener("load", reqListener);
oReq.open("GET", "http://www.example.org/example.txt");
oReq.send();
*/
